﻿namespace ExercicioCronometro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ButtonOnOff = new System.Windows.Forms.Button();
            this.LabelContador = new System.Windows.Forms.Label();
            this.TimerRelogto = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ButtonOnOff
            // 
            this.ButtonOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonOnOff.Location = new System.Drawing.Point(243, 134);
            this.ButtonOnOff.Name = "ButtonOnOff";
            this.ButtonOnOff.Size = new System.Drawing.Size(131, 65);
            this.ButtonOnOff.TabIndex = 0;
            this.ButtonOnOff.Text = "Liga";
            this.ButtonOnOff.UseVisualStyleBackColor = true;
            this.ButtonOnOff.Click += new System.EventHandler(this.ButtonOnOff_Click);
            // 
            // LabelContador
            // 
            this.LabelContador.AutoSize = true;
            this.LabelContador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelContador.Location = new System.Drawing.Point(110, 79);
            this.LabelContador.Name = "LabelContador";
            this.LabelContador.Size = new System.Drawing.Size(80, 16);
            this.LabelContador.TabIndex = 1;
            this.LabelContador.Text = "00:00:00 000";
            // 
            // TimerRelogto
            // 
            this.TimerRelogto.Interval = 30;
            this.TimerRelogto.Tick += new System.EventHandler(this.TimerRelogto_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 261);
            this.Controls.Add(this.LabelContador);
            this.Controls.Add(this.ButtonOnOff);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonOnOff;
        private System.Windows.Forms.Label LabelContador;
        private System.Windows.Forms.Timer TimerRelogto;
    }
}

