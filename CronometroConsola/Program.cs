﻿using ExercicioCronometro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CronometroConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();
            Console.WriteLine("Pressione Enter para iniciar o cronometro|");
            Console.ReadLine();

            Console.WriteLine("Pressione Enter novamente para parar o cronometro|");
            relogio.StartClock();
            while (relogio.ClockState())
            {
                var tempo = DateTime.Now - relogio.StartTime();
                Console.Write("\r Tempo:{0}", tempo);
                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key== ConsoleKey.Enter)
                    {
                        break;
                    }
                }
               
            }
            relogio.StopClock();
            Console.WriteLine("\r Tempo Cronometrado:{0}", relogio.GetTimeSpan());
            Console.ReadLine();
        }
    }
}
